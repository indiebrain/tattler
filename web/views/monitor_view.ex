defmodule Tattler.MonitorView do
  use Tattler.Web, :view

  def monitor_frequency_menu_options do
    Enum.reduce(
      Tattler.Monitor.frequencies,
      %{},
      fn({key, value}, accum) ->
        Map.put(
          accum,
          monitor_frequency_display(key),
          value
        )
      end
    )
  end

  def monitor_frequency_display(frequency) do
    humanize(frequency)
  end
end
