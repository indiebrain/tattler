defmodule Tattler.Ping do
  use Tattler.Web, :model

  schema "pings" do
    field :status, :string
    field :response_time, :float
    belongs_to :monitor, Tattler.Monitor

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:status])
    |> validate_required([:status])
  end

  @statuses Enum.reduce(
    [
      :success,
      :failure
    ],
    %{},
    fn atom, map ->
      Map.put(map, atom, Atom.to_string(atom))
    end
  )
  def statuses do
    @statuses
  end
end
