defmodule Tattler.Monitor do
  use Tattler.Web, :model

  schema "monitors" do
    field :name, :string
    field :url, :string
    field :frequency, :string
    belongs_to :user, Tattler.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :url, :frequency])
    |> validate_required([:name, :url, :frequency])
  end

  @frequencies Enum.reduce(
    [
      :every_minute,
      :every_hour,
      :every_day
    ],
    %{},
    fn atom, map ->
      Map.put(map, atom, Atom.to_string(atom))
    end
  )

  def frequencies do
    @frequencies
  end
end
