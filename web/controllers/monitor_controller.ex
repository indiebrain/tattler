defmodule Tattler.MonitorController do
  use Tattler.Web, :controller

  alias Tattler.Monitor
  alias Tattler.Authentication

  def index(conn, _params) do
    user = Authentication.current_user(conn)
    monitors = Monitor
      |> where(user_id: ^user.id)
      |> Repo.all

    conn
    |> render("index.html", monitors: monitors)
  end

  def new(conn, _params) do
    changeset = Monitor.changeset(%Monitor{}, %{})

    conn
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{ "monitor" => monitor_params}) do
    changeset = Monitor.changeset(%Monitor{}, monitor_params)
    changeset = Ecto.Changeset.put_assoc(changeset, :user, Authentication.current_user(conn))

    case Repo.insert(changeset) do
      { :ok, monitor } ->
        conn
        |> put_flash(:info, "Created monitor: #{monitor.name}")
        |> redirect(to: monitor_path(conn, :index))
      { :error, changeset } ->
        conn
        |> put_status(422)
        |> put_flash(:error, "Monitor creation failed")
        |> render("new.html", changeset: changeset)
    end
  end

  def edit(conn, %{ "id" => monitor_id }) do
    monitor = Repo.get(Monitor, monitor_id)
    changeset = Monitor.changeset(monitor)

    conn
    |> render("edit.html", monitor: monitor, changeset: changeset)
  end

  def update(conn, %{ "id" => id, "monitor" => monitor_params }) do
    monitor = Repo.get_by(Monitor, id: id, user_id: Authentication.current_user(conn).id)
    changeset = Monitor.changeset(monitor, monitor_params)

    case Repo.update(changeset) do
      { :ok, monitor } ->
        conn
        |> put_flash(:info, "Updated: #{monitor.name}")
        |> redirect(to: monitor_path(conn, :index))
      {:error, changeset} ->
        conn
        |> put_status(422)
        |> render("edit.html", monitor: monitor, changeset: changeset)
    end
  end

  def delete(conn, %{ "id" => id }) do
    monitor = Repo.get_by(Monitor, id: id, user_id: Authentication.current_user(conn).id)

    Repo.delete!(monitor)

    conn
    |> put_flash(:info, "Deleted: #{monitor.name}")
    |> redirect(to: monitor_path(conn, :index))
  end
end
