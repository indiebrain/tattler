defmodule Tattler.SessionController do
  use Tattler.Web, :controller

  alias Tattler.Authentication

  def new(conn, _params) do
    conn
    |> render("new.html")
  end

  def create(conn, %{ "session" => %{ "email" => email, "password" => password } }) do
    case Authentication.login_user(conn, email, password) do
      { :ok, user, conn } ->
        conn
        |> Authentication.authenticate_user(user)
        |> put_flash(:info, "Welcome back")
        |> redirect(to: monitor_path(conn, :index))
      { :error, _reason, conn } ->
        conn
        |> put_status(401)
        |> put_flash(:error, "Invalid user credentials")
        |> render("new.html")
    end
  end

  def delete(conn, _) do
    conn
    |> Authentication.logout()
    |> redirect(to: page_path(conn, :index))
  end
end
