defmodule Tattler.UserController do
  use Tattler.Web, :controller

  alias Tattler.User
  alias Tattler.Authentication

  def new(conn, _params) do
    changeset = User.changeset(%User{}, %{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{ "user" => user_attributes }) do
    changeset = User.registration_changeset(%User{}, user_attributes)

    case Repo.insert(changeset) do
      {:ok, user } ->
        conn
        |> Authentication.authenticate_user(user)
        |> put_flash(:info, "Registration successful")
        |> redirect(to: monitor_path(conn, :index))
      { :error, changeset } ->
        conn
        |> put_status(422)
        |> put_flash(:error, "Registration failed")
        |> render("new.html", changeset: changeset)
    end
  end
end
