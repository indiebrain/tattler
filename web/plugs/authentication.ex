defmodule Tattler.Authentication do
  import Plug.Conn

  import Comeonin.Bcrypt, only: [
    checkpw: 2,
    dummy_checkpw: 0
  ]

  def init(opts) do
    Keyword.fetch!(opts, :repo)
  end

  def call(conn, repo) do
    user_id = get_session(conn, :user_id)
    cond do
      user = conn.assigns[:current_user] ->
        conn
      user = user_id && repo.get(Tattler.User, user_id) ->
        assign(conn, :current_user, user)
      true ->
        assign(conn, :current_user, nil)
    end
  end

  import Phoenix.Controller
  alias Tattler.Router.Helpers
  def require_authenticated_user(conn, _opts) do
    if conn.assigns.current_user do
      conn
    else
      conn
      |> put_flash(:error, "You must be logged in to view that")
      |> redirect(to: Helpers.page_path(conn, :index))
      |> halt()
    end
  end

  def authenticate_user(conn, user) do
    conn
    |> assign(:current_user, user)
    |> put_session(:user_id, user.id)
    |> configure_session(renew: true)
  end

  def login_user(conn, email, password) do
    user = Tattler.Repo.get_by(Tattler.User, email: email)

    cond do
      user && checkpw(password, user.password_hash) ->
        { :ok, user, conn }
      user ->
        { :error, :unauthorized, conn }
      true ->
        dummy_checkpw()
        { :error, :not_found, conn }
    end
  end

  def logout(conn) do
    conn
    |> assign(:current_user, nil)
    |> put_session(:user_id, nil)
    |> configure_session(drop: true)
  end

  def current_user(conn) do
    conn.assigns.current_user
  end

  def logged_in?(conn) do
    !!current_user(conn)
  end
end
