defmodule Tattler.Router do
  use Tattler.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Tattler.Authentication, repo: Tattler.Repo
  end

  pipeline :browser_authenticated do
    plug :require_authenticated_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Tattler do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    resources "/users", UserController, only: [ :new, :create ]
    resources "/sessions", SessionController, only: [ :new, :create, :delete ]
  end

  scope "/", Tattler do
    pipe_through [ :browser, :browser_authenticated ]

    resources "/monitors", MonitorController, only: [ :index, :new, :create, :edit, :update, :delete ]
  end


  # Other scopes may use custom stacks.
  # scope "/api", Tattler do
  #   pipe_through :api
  # end
end
