use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :tattler, Tattler.Endpoint,
  http: [port: 4001],
  server: true

config :tattler, :sql_sandbox, true

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :tattler, Tattler.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "tattler_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
