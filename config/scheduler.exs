use Mix.Config

config :quantum, :tattler,
  cron: [
    # Every minute
    "* * * * *": fn -> Tattler.MonitorPingJob.enqueue_all(Tattler.Monitor.frequencies.every_minute) end
  ]
