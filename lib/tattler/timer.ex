defmodule Tattler.Timer do
  @doc """
  Measures the execution time of the function in ms.

  Returns: A tuple containing the execution time in ms and the
  function's result.

  Examples:

  {time, result} = Tattler.Timer.measure(fn -> Process.sleep(1) end

  """
  def measure(function) do
    { execution_time, result } = :timer.tc(function)
    execution_time_in_ms = (execution_time / 1_000)
    { execution_time_in_ms, result}
  end
end
