defmodule Tattler.MonitorPingJob do

  @queue "default"
  @options [
    max_retries: 0
  ]

  require Logger
  alias Tattler.Repo
  import Ecto.Query

  def enqueue_all(frequency) do
    monitors = Tattler.Monitor
    |> select([:id])
    |> where(frequency: ^frequency)
    |> Repo.all

    Enum.map(monitors, fn(monitor) -> enqueue(monitor.id) end)
  end

  def enqueue(monitor_id) do
    Exq.enqueue(Exq, @queue, Tattler.MonitorPingJob, [monitor_id], @options)
  end

  def perform(monitor_id) do
    Logger.debug("#{__MODULE__} performing for %Monitor{id: #{monitor_id}}...")
    monitor = Tattler.Repo.get!(Tattler.Monitor, monitor_id)

    { response_time, http_response } = Tattler.Timer.measure(
      fn ->
        HTTPotion.get(monitor.url, headers: ["User-Agent": "Tattler Ping Bot"])
      end
    )

    case http_response do
      %HTTPotion.Response{status_code: 200} ->
        record_ping(monitor, Tattler.Ping.statuses.success, response_time)
      _ ->
        record_ping(monitor, Tattler.Ping.statuses.failure, response_time)
    end
  end

  defp record_ping(monitor, status, response_time) do
    Logger.debug("#{__MODULE__} recording ping instance %Ping{monitor_id: #{monitor.id}, status: #{status}}...")
    Tattler.Repo.insert!(
      %Tattler.Ping{
        status: status,
        monitor_id: monitor.id,
        response_time: response_time
      }
    )
  end
end
