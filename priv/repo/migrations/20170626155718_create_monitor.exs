defmodule Tattler.Repo.Migrations.CreateMonitor do
  use Ecto.Migration

  def change do
    create table(:monitors) do
      add :name, :text, null: false
      add :url, :text, null: false
      add :frequency, :string, null: false
      add :user_id, references(:users, on_delete: :nothing), null: false

      timestamps()
    end
    create index(:monitors, [:user_id])

  end
end
