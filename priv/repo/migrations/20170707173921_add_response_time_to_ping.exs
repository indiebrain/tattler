defmodule Tattler.Repo.Migrations.AddResponseTimeToPing do
  use Ecto.Migration

  def change do
    alter table(:pings) do
      add :response_time, :float
    end
  end
end
