defmodule Tattler.Repo.Migrations.CreatePing do
  use Ecto.Migration

  def change do
    create table(:pings) do
      add :status, :string
      add :monitor_id, references(:monitors, on_delete: :nothing), null: false

      timestamps()
    end
    create index(:pings, [:monitor_id])

  end
end
