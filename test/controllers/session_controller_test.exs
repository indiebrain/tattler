defmodule Tattler.SessionControllerTest do
  use Tattler.ConnCase

  test "GET /sessions/new", %{ conn: conn } do
    response = get(conn, "/sessions/new")

    assert(html_response(response, 200) =~ "Login")
  end

  test "POST /sessions with valid credentials creates a session", %{ conn: conn } do
    user = insert(:user)
    attributes = %{
      email: user.email,
      password: user.password
    }

    response = post(conn, "/sessions", %{ "session" => attributes})

    assert(redirected_to(response) == monitor_path(conn, :index))
    assert(get_session(response, :user_id) == user.id)
    assert(response.assigns[:current_user].id == user.id)
  end

  test "POST /sessions with invalid credentials does not create a session", %{ conn: conn } do
    user = build(:user)
    attributes = %{
      email: user.email,
      password: user.password
    }

    response = post(conn, "/sessions", %{ "session" => attributes})

    assert(response.status == 401)
    assert(get_session(response, :user_id) == nil)
    assert(response.assigns[:current_user] == nil)
  end

  test "DELETE /sessions destroys the session", %{ conn: conn } do
    user = build(:user, id: "1")
    login_user(conn, user)


    response = delete(conn, session_path(conn, :delete, user))

    assert(redirected_to(response, 302) == page_path(conn, :index))
    assert(get_session(response, :user_id) == nil)
    assert(response.assigns.current_user == nil)
  end
end
