defmodule Tattler.UserControllerTest do
  use Tattler.ConnCase

  test "GET /users/new ", %{ conn: conn } do
    response = get(conn, "/users/new")

    assert(html_response(response, 200) =~ "Register")
  end

  test "POST /users redirects when given valid attributes", %{ conn: conn } do
    valid_attributes = %{
      email: "email@example.com",
      password: "valid-password",
      password_confirmation: "valid-password"
    }

    response = post(conn, "/users", %{ user:  valid_attributes })

    assert(response.status == 302)
    assert({"location", "/monitors"} in response.resp_headers)
  end

  test "POST /users with invalid attributes", %{ conn: conn } do
    invalid_attributes = %{ email: nil }

    response = post(conn, "/users", %{ user: invalid_attributes })

    assert(html_response(response, 422) =~ "Register")
  end
end
