defmodule Tattler.MonitorControllerTest do
  use Tattler.ConnCase

  alias Tattler.Monitor

  test "requires authentication", %{ conn: conn } do
    Enum.each(
      [
        get(conn, "/monitors"),
        get(conn, "/monitors/new"),
        post(conn,"/monitors", %{})
      ],
      fn response ->
        assert redirected_to(response) =~ page_path(conn, :index)
        assert get_flash(response, :error) =~ "You must be logged in to view that"
      end
    )
  end

  test "GET /monitors", %{ conn: conn } do
    user = insert(:user)
    monitor = insert(:monitor, user: user)
    monitor = Repo.get(Monitor, monitor.id)
    other_user = insert(:user)
    other_monitor = insert(:monitor, user: other_user)
    conn = login_user(conn, user)

    response = get(conn, "/monitors")

    assert monitor in response.assigns.monitors
    refute other_monitor in response.assigns.monitors
    assert html_response(response, 200) =~ "Monitors"
  end

  test "GET /monitors/new", %{ conn: conn } do
    user = insert(:user)
    conn = login_user(conn, user)

    response = get(conn, "/monitors/new")

    assert html_response(response, 200) =~ "Monitor"
  end

  test "POST /monitors with valid attributes", %{ conn: conn } do
    user = insert(:user)
    conn = login_user(conn, user)
    attributes = params_for(:monitor)

    response = post(conn, "/monitors", %{ "monitor" => attributes })

    assert(html_response(response, 302))
    assert({"location", "/monitors"} in response.resp_headers)
  end

  test "POST /monitors with invalid attributes", %{ conn: conn } do
    user = insert(:user)
    conn = login_user(conn, user)

    response = post(conn, "/monitors", %{ "monitor" => %{} })

    assert(html_response(response, 422) =~ "Monitor creation failed")
  end

  test "GET /monitors/:id/edit", %{ conn: conn } do
    user = insert(:user)
    monitor = insert(:monitor, user: user)
    conn = login_user(conn, user)

    response = get(conn, "/monitors/#{monitor.id}/edit")

    assert(html_response(response, 200) =~ "Edit #{monitor.name}")
  end

  test "PUT /monitors/:id", %{ conn: conn } do
    user = insert(:user)
    monitor = insert(:monitor, user: user)
    conn = login_user(conn, user)
    attributes = params_for(:monitor)

    response = put(conn, "/monitors/#{monitor.id}", monitor: attributes)

    assert(html_response(response, 302))
    assert({"location", "/monitors"} in response.resp_headers)
  end

  test "PUT /monitors/:id with invalid attributes", %{ conn: conn } do
    user = insert(:user)
    monitor = insert(:monitor, user: user)
    conn = login_user(conn, user)

    response = put(conn, "/monitors/#{monitor.id}", monitor: %{ name: nil })

    assert(html_response(response, 422) =~ "Edit #{monitor.name}")
  end

  test "DELETE /monitors/:id", %{ conn: conn } do
    user = insert(:user)
    monitor = insert(:monitor, user: user)
    conn = login_user(conn, user)

    response = delete(conn, "/monitors/#{monitor.id}")

    assert(html_response(response, 302))
    assert({"location", "/monitors"} in response.resp_headers)
  end
end
