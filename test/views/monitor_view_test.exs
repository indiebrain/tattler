defmodule Tattler.MonitorViewTest do
  use ExUnit.Case

  alias Tattler.MonitorView

  test "#monitor_frequency_menu_options" do
    expected = %{
      "Every day" => "every_day",
      "Every hour" => "every_hour",
      "Every minute" => "every_minute"
    }

    assert(
      MonitorView.monitor_frequency_menu_options ==
        expected
    )
  end

  test "#monitor_frequency_display" do
    assert(
      MonitorView.monitor_frequency_display("frequency_option") ==
        "Frequency option"
    )
  end
end
