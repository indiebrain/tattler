defmodule Tattler.AuthenticationTest do
  use Tattler.ConnCase
  alias Tattler.Authentication

  setup %{conn: conn} do
    conn =
      conn
      |> bypass_through(Tattler.Router, :browser)
      |> get("/")
    { :ok, %{ conn: conn } }
  end

  test "#call returns the connection when the connection has a :current_user asigned", %{ conn: conn } do
    conn =
      conn
      |> assign(:current_user, %Tattler.User{})

    assert(Authentication.call(conn, Tattler.Repo) == conn)
  end

  test "#call assigns the current user when the session contains a vaild user id", %{ conn: conn } do
    user = insert(:user)
    conn =
      conn
      |> put_session(:user_id, user.id)
      |> Authentication.call(Tattler.Repo)

    assert(conn.assigns.current_user.id == user.id)
  end

  test "#call assigns nil as the current user when the session does not contain a valid id", %{ conn: conn } do
    conn =
      conn
      |> put_session(:user_id, "1")
      |> Authentication.call(Tattler.Repo)

    assert(conn.assigns.current_user == nil)
  end

  test "#require_authenticated_user continues when the current_user is presnet", %{ conn: conn } do
    conn =
      conn
      |> assign(:current_user, %Tattler.User{})
      |> Authentication.require_authenticated_user(Tattler.Repo)

    refute(conn.halted())
  end

  test "#require_authenticated_user halts when the current_user is absent", %{ conn: conn } do
    conn =
      conn
      |> Authentication.require_authenticated_user(Tattler.Repo)

    assert(conn.halted())
  end

  test "#authenticate_user assigns the current user", %{ conn: conn } do
    user = build(:user)
    conn =
      conn
      |> Authentication.authenticate_user(user)

    assert(conn.assigns.current_user == user)
  end

  test "#authenticate_user configures the user session", %{ conn: conn } do
    user = build(:user, id: "id")
    conn =
      conn
      |> Authentication.authenticate_user(user)

    assert(get_session(conn, :user_id) == user.id)
  end

  test "#login_user returns {:ok, user, conn} when the user credentials are valid", %{ conn: conn } do
    user = insert(:user)

    result = Authentication.login_user(conn, user.email, user.password)

    assert({ :ok, _user, _conn } = result)
  end

  test "#login_user returns {:error, :unauthorized, conn} when given an invalid password", %{ conn: conn } do
    user = insert(
      :user,
      password: "valid-password",
      password_confirmation: "valid-password"
    )

    result = Authentication.login_user(conn, user.email, "not-user-password")

    assert({ :error, :unauthorized, _conn } = result)
  end

  test "#login_user returns {:error, :not_found, conn} when given an invalid user email address", %{ conn: conn } do
    result = Authentication.login_user(conn, "email@example.com", "not-user-password")

    assert({ :error, :not_found, _conn } = result)
  end

  test "#logout unassigns the current_user", %{ conn: conn } do
    conn =
      conn
      |> assign(:current_user, %Tattler.User{})
      |> Authentication.logout

    assert(conn.assigns.current_user == nil)
  end

  test "#logout destorys the user session", %{ conn: conn } do
    conn =
      conn
      |> put_session(:user_id, "user-id")
      |> Authentication.logout

    assert(get_session(conn, :user_id) == nil)
  end

  test "#current_user is the connection's current_user assigns", %{ conn: conn } do
    user = build(:user)
    conn = conn
      |> assign(:current_user, user)

    assert(Authentication.current_user(conn) == user)
  end

  test "#logged_in? is true if the connection's current_user is assigned", %{ conn: conn } do
    user = build(:user)
    conn = conn
      |> assign(:current_user, user)

    assert(Authentication.logged_in?(conn) == true)
  end

  test "#logged_in? is false if the connection's current_user is not assigned", %{ conn: conn } do
    conn = conn
      |> assign(:current_user, nil)

    assert(Authentication.logged_in?(conn) == false)
  end
end
