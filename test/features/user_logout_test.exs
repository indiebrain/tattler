defmodule Tattler.UserLogoutTest do
  use Tattler.FeatureCase

  test "User can logout", %{ session: session } do
    session
    |> login_user(insert(:user))
    |> expect_login_link_to_be_absent
    |> logout
    |> expect_login_link_to_be_present
  end

  defp logout(session) do
    session
    |> click(HomePage.logout_link)
  end

  defp expect_login_link_to_be_present(session) do
    session
    |> assert_has(HomePage.login_link)
  end

  defp expect_login_link_to_be_absent(session) do
    session
    |> refute_has(HomePage.login_link)
  end
end
