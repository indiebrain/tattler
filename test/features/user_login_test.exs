defmodule Tattler.UserLoginTest do
  use Tattler.FeatureCase

  test "User can login", %{ session: session } do
    user = insert(:user)

    session
    |> visit_homepage
    |> navigate_to_user_login_form
    |> fill_in_user_login_form(user)
    |> expect_success_message_to_be_present
  end

  defp visit_homepage(session) do
    session
    |> visit("/")
  end

  defp navigate_to_user_login_form(session) do
    session
    |> click(HomePage.login_link)
  end

  defp fill_in_user_login_form(session, user) do
    session
    |> fill_in(UserLoginForm.email_input, with: user.email)
    |> fill_in(UserLoginForm.password_input, with: user.password)
    |> click(UserLoginForm.submit_button)
  end

  defp expect_success_message_to_be_present(session) do
    session
    |> assert_has(UserLoginForm.successful_login_flash_message)
  end
end
