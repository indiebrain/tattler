defmodule MonitorDeleteTest do
  use Tattler.FeatureCase

  test "User deletes a monitor", %{ session: session } do
    user = insert(:user)
    monitor = insert(
      :monitor,
      name: "Example Monitor",
      user: user
    )

    session
    |> login_user(user)
    |> delete_monitor(monitor)
    |> expect_monitor_row_to_be_absent(monitor)
    |> expect_deletion_message_to_be_present(monitor)
  end

  defp delete_monitor(session, monitor) do
    session
    |> accept_confirm(fn(session) -> click(session, MonitorIndexPage.monitor_delete_button(monitor)) end)
    session
  end

  defp expect_monitor_row_to_be_absent(session, monitor) do
    session
    |> refute_has(MonitorIndexPage.monitor_row(monitor))
  end

  defp expect_deletion_message_to_be_present(session, monitor) do
    session
    |> assert_has(MonitorIndexPage.deletion_success_message(monitor))
  end
end
