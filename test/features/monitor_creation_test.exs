defmodule Tattler.MonitorCreationTest do
  use Tattler.FeatureCase

  test "User creates a monitor", %{ session: session } do
    user = insert(:user)

    session
    |> login_user(user)
    |> navigate_to_new_monitor_form
    |> fill_out_form
    |> submit_form
    |> expect_success_message_to_be_present
  end

  defp navigate_to_new_monitor_form(session) do
    session
    |> click(MonitorIndexPage.new_monitor_link)
  end

  defp fill_out_form(session) do
    session
    |> fill_in(MonitorForm.name_input, with: "Test Monitor")
    |> fill_in(MonitorForm.url_input, with: "http://www.example.com")
    |> click(MonitorForm.every_hour_dropdown_option)
  end

  defp submit_form(session) do
    session
    |> click(MonitorForm.save_button)
  end

  defp expect_success_message_to_be_present(session) do
    session
    |> assert_has(MonitorForm.successful_save_flash_message)
  end
end
