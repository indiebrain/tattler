defmodule MonitorEditTest do
  use Tattler.FeatureCase

  test "User can edit a monitor", %{ session: session } do
    user = insert(:user)
    monitor = insert(
      :monitor,
      name: "Example Monitor",
      user: user
    )

    session
    |> login_user(user)
    |> edit_monitor(monitor)
    |> fill_out_form
    |> expect_success_message_to_be_present
  end

  defp edit_monitor(session, monitor) do
    session
    |> click(MonitorIndexPage.monitor_edit_button(monitor))
  end

  defp fill_out_form(session) do
    session
    |> fill_in(MonitorForm.name_input, with: "New Monitor")
    |> click(MonitorForm.save_button)
  end

  defp expect_success_message_to_be_present(session) do
    session
    |> assert_has(MonitorForm.successful_update_flash_message)
  end
end
