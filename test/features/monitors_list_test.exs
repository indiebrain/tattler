defmodule Tattler.MonitorsListTest do
  use Tattler.FeatureCase

  test "User views a list of monitors", %{ session: session } do
    user = insert(:user)
    monitor = insert(:monitor, user: user)

    session
    |> login_user(user)
    |> expect_monitor_row_to_be_present(monitor)
  end

  defp expect_monitor_row_to_be_present(session, monitor) do
    session
    |> assert_has(MonitorIndexPage.monitor_row(monitor))
  end
end
