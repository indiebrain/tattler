defmodule Tattler.UserRegistrationTest do
  use Tattler.FeatureCase

  test "User can register", %{ session: session } do
    session
    |> visit_homepage
    |> navigate_to_user_registration_form
    |> fill_in_user_registration_form
    |> submit_form
    |> expect_success_message_to_be_present
    |> expect_monitors_table_to_be_present
  end

  defp visit_homepage(session) do
    session
    |> visit("/")
  end

  defp navigate_to_user_registration_form(session) do
    session
    |> click(HomePage.user_registration_link)
  end

  defp fill_in_user_registration_form(session) do
    session
    |> fill_in(UserRegistrationForm.email_input, with: "email@example.com")
    |> fill_in(UserRegistrationForm.password_input, with: "valid-password")
    |> fill_in(UserRegistrationForm.password_confirmation_input, with: "valid-password")
  end

  defp submit_form(session) do
    session
    |> click(UserRegistrationForm.submit_button)
  end

  defp expect_success_message_to_be_present(session) do
    session
    |> assert_has(UserRegistrationForm.successful_save_flash_message)
  end

  defp expect_monitors_table_to_be_present(session) do
    session
    |> assert_has(MonitorIndexPage.monitor_table)
  end
end
