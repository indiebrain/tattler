defmodule MonitorForm do

  import Wallaby.Query, only: [
    button: 1,
    css: 2,
    option: 1,
    text_field: 1,
  ]

  def name_input do
    text_field("Name")
  end

  def url_input do
    text_field("URL")
  end

  def every_hour_dropdown_option do
    option("Every hour")
  end

  def save_button do
    button("Save")
  end

  def successful_save_flash_message do
    css(".alert-info", text: "Created monitor: Test Monitor")
  end

  def successful_update_flash_message do
    css(".alert-info", text: "Updated: New Monitor")
  end
end
