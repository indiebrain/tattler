defmodule HomePage do

  import Wallaby.Query, only: [
    link: 1
  ]

  def login_link do
    link("Login")
  end

  def logout_link do
    link("Logout")
  end

  def user_registration_link do
    link("Register")
  end
end
