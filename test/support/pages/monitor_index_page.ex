defmodule MonitorIndexPage do

  import Wallaby.Query, only: [
    css: 1,
    css: 2,
    link: 1,
  ]

  def monitor_table do
    css("table#monitors")
  end

  def new_monitor_link do
    link("New Monitor")
  end

  def monitor_row(monitor) do
    css("#monitor-#{monitor.id}")
  end

  def monitor_edit_button(monitor) do
    css("#monitor-#{monitor.id} [data-role='edit-button']")
  end

  def monitor_delete_button(monitor) do
    css("#monitor-#{monitor.id} [data-role='delete-button']")
  end

  def deletion_success_message(monitor) do
    css(".alert-info", text: "Deleted: #{monitor.name}")
  end
end
