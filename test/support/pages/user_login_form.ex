defmodule UserLoginForm do

  import Wallaby.Query, only: [
    button: 1,
    css: 2,
    text_field: 1,
  ]

  def email_input do
    text_field("Email")
  end

  def password_input do
    text_field("Password")
  end

  def submit_button do
    button("Login")
  end

  def successful_login_flash_message do
    css(".alert-info", text: "Welcome back")
  end
end
