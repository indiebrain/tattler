defmodule UserRegistrationForm do

  import Wallaby.Query, only: [
    button: 1,
    css: 2,
    text_field: 1
  ]

  def email_input do
    text_field("Email")
  end

  def password_input do
    text_field("Password")
  end

  def password_confirmation_input do
    text_field("Password confirmation")
  end

  def submit_button do
    button("Register")
  end

  def successful_save_flash_message do
    css(".alert-info", text: "Registration successful")
  end
end
