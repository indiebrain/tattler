defmodule Tattler.FeatureCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      use Wallaby.DSL

      alias Tattler.Repo
      import Ecto
      import Ecto.Changeset
      import Ecto.Query
      import Tattler.Factory
      import Tattler.Router.Helpers
      import Tattler.FeatureCase
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Tattler.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Tattler.Repo, {:shared, self()})
    end

    metadata = Phoenix.Ecto.SQL.Sandbox.metadata_for(Tattler.Repo, self())
    {:ok, session} = Wallaby.start_session(metadata: metadata)
    {:ok, session: session}
  end

  use Wallaby.DSL
  def login_user(session, %Tattler.User{ email: email, password: password }) do
    session
    |> visit("/sessions/new")
    |> fill_in(Wallaby.Query.text_field("Email"), with: email)
    |> fill_in(Wallaby.Query.text_field("Password"), with: password)
    |> click(Wallaby.Query.button("Login"))
  end
end
