defmodule Tattler.Factory do

  use ExMachina.Ecto, repo: Tattler.Repo

  def user_factory do
    %Tattler.User{
      email: sequence(:email, &"email-#{&1}@example.com"),
      password: "valid-password",
      password_hash: Tattler.User.hash_password("valid-password")
    }
  end

  def monitor_factory do
    %Tattler.Monitor{
      name: "Monitor Name",
      url: "http://www.example.com/heartbeat",
      frequency: Tattler.Monitor.frequencies.every_minute
    }
  end
end
