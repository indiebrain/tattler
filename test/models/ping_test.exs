defmodule Tattler.PingTest do
  use Tattler.ModelCase

  alias Tattler.Ping

  @valid_attrs %{status: "some content"}
  @invalid_attrs %{}

  test "#changeset with valid attributes" do
    changeset = Ping.changeset(%Ping{}, @valid_attrs)
    assert changeset.valid?
  end

  test "#changeset with invalid attributes" do
    changeset = Ping.changeset(%Ping{}, @invalid_attrs)
    refute changeset.valid?
  end
end
