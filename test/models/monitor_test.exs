defmodule Tattler.MonitorTest do
  use Tattler.ModelCase

  alias Tattler.Monitor

  @valid_attrs %{frequency: "some content", name: "some content", url: "some content"}
  @invalid_attrs %{}

  test "#changeset with valid attributes" do
    changeset = Monitor.changeset(%Monitor{}, @valid_attrs)
    assert changeset.valid?
  end

  test "#changeset with invalid attributes" do
    changeset = Monitor.changeset(%Monitor{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "#frequencies.every_minute" do
    assert(
      Monitor.frequencies.every_minute == "every_minute"
    )
  end

  test "#frequencies.every_hour" do
    assert(
      Monitor.frequencies.every_hour == "every_hour"
    )
  end

  test "#frequencies.every_day" do
    assert(
      Monitor.frequencies.every_day == "every_day"
    )
  end
end
