defmodule Tattler.UserTest do
  use Tattler.ModelCase

  alias Tattler.User

  test "#registration_changeset with valid attributes" do
    attributes = %{
      email: "email@example.com",
      password: "valid-password",
      password_confirmation: "valid-password"
    }

    changeset = User.registration_changeset(%User{}, attributes)

    assert changeset.valid?
  end

  test "#registration_changeset with empty password" do
    attributes = %{
      email: "email@example.com",
      password: "",
      password_confirmation: ""
    }

    changeset = User.registration_changeset(%User{}, attributes)

    refute changeset.valid?
    assert [password: "can't be blank"] == errors_on(changeset)
  end

  test "#registration_changeset with malformed email address" do
    attributes = %{
      email: "invalid-email-address",
      password: "valid-password",
      password_confirmation: "valid-password"
    }

    changeset = User.registration_changeset(%User{}, attributes)

    refute changeset.valid?
    assert [email: "has invalid format"] == errors_on(changeset)
  end

  test "#registration_changeset with mismatched password and password_confiration" do
    attributes = %{
      email: "email@example.com",
      password: "valid-password",
      password_confirmation: "invalid-password-confirmation"
    }

    changeset = User.registration_changeset(%User{}, attributes)

    refute changeset.valid?
    assert [password_confirmation: "does not match confirmation"] == errors_on(changeset)
  end

  test "#registration_changeset with short password" do
    attributes = %{
      email: "email@example.com",
      password: "pass",
      password_confirmation: "pass"
    }

    changeset = User.registration_changeset(%User{}, attributes)


    refute changeset.valid?
    assert [password: "should be at least 8 character(s)"] == errors_on(changeset)
  end
end
