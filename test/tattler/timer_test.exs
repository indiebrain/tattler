defmodule Tattler.TimerTest do
  use ExUnit.Case

  test "measure returns a tuple of the execution time and function result" do
    {time, result} = Tattler.Timer.measure(fn -> Process.sleep(1) end)
    assert(is_float(time))
    assert(result == :ok)
  end
end
