defmodule Tattler.MonitorPingJobTest do
  use Tattler.JobCase

  import Mock

  test "#enqueue creates a background job" do
    monitor = insert(:monitor, user: insert(:user))
    Tattler.MonitorPingJob.enqueue(monitor.id)

    {
      :ok,
      [
        %Exq.Support.Job{
          jid: job_id,
          args: [monitor_id],
          class: enqueued_job_module,
          retry: retries,
          queue: queue}
      ]
    } = Exq.Api.jobs(Exq.Api, "default")
    Exq.Api.remove_job(Exq.Api, queue, job_id)

    assert(monitor.id == monitor_id)
    assert("Tattler.MonitorPingJob" == enqueued_job_module)
    assert(0 == retries)
    assert("default" == queue)
  end

  test "#perform records a successful ping" do
    with_mock(
      HTTPotion,
      [
        get: fn("http://example.com", _options) -> %HTTPotion.Response{ status_code: 200 } end
      ]
    ) do
      monitor = insert(
        :monitor,
        user: insert(:user),
        url: "http://example.com"
      )
      Tattler.MonitorPingJob.perform(monitor.id)

      [ ping | _ ] = Tattler.Ping
        |> order_by(desc: :inserted_at)
        |> limit(1)
        |> Repo.all

      assert(ping.monitor_id == monitor.id)
      assert(ping.status == Tattler.Ping.statuses.success)
      assert(ping.response_time != nil)
    end
  end

  test "#perform records an unsuccessful ping" do
    with_mock(
      HTTPotion,
      [
        get: fn("http://example.com", _options) -> %HTTPotion.Response{ status_code: 404 } end
      ]
    ) do
      monitor = insert(
        :monitor,
        user: insert(:user),
        url: "http://example.com"
      )
      Tattler.MonitorPingJob.perform(monitor.id)

      [ ping | _ ] = Tattler.Ping
        |> order_by(desc: :inserted_at)
        |> limit(1)
        |> Repo.all

      assert(ping.monitor_id == monitor.id)
      assert(ping.status == Tattler.Ping.statuses.failure)
      assert(ping.response_time != nil)
    end
  end
end
